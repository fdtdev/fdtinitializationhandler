﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.InitializationHandler
{
    public class ScriptableObjectsInitializer : InitWrapperItemBase
    {
        [SerializeField] protected List<ScriptableObject> _listObjects = new List<ScriptableObject>();

        public override void Init()
        {
            for (int i = 0; i < _listObjects.Count; i++)
            {
                if (_listObjects[i] is IInitializable)
                {
                    (_listObjects[i] as IInitializable).Init();
                }
            }
            FinishInit();
        }
    }
}