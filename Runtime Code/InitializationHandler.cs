﻿using System.Collections.Generic;
using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.InitializationHandler
{
    public class InitializationHandler : MonoBehaviour, IInitWrapper
    {
	    [SerializeField] protected List<InitWrapperItemBase> _initializationItems = new List<InitWrapperItemBase>();
        // Use this for initialization

        public bool autorun = false;
	    protected List<InitWrapperItemBase> _toInitialize;
	    protected HashSet<InitWrapperItemBase> _initializingItems;
	    protected HashSet<InitWrapperItemBase> _initializedItems;
        protected int initializeCount = -1;
        [SerializeField, HideInInspector] protected MultiEvent _endEvt;
	    void Start () {
            if (autorun)
		        Init();
	    }
	
	    public void Init()
	    {
		    _toInitialize = new List<InitWrapperItemBase>(_initializationItems);
		    _initializingItems = new HashSet<InitWrapperItemBase>();
		    _initializedItems = new HashSet<InitWrapperItemBase>();
            for (int i = _toInitialize.Count-1; i>=0; i--)
            {
                if (!_toInitialize[i].IsEnabled)
                {
                    _toInitialize.RemoveAt(i);
                }
                else
                {
                    _toInitialize[i].ComputeRuntimeDependences();
                    _toInitialize[i].initState = InitWrapperItemBase.InitState.NOT_INITIALIZED;
                    _toInitialize[i].availableState = InitWrapperItemBase.AvailableState.AVAILABLE;    
                }
            }

            initializeCount = _toInitialize.Count;
            ExecuteItems();
	    }
        protected void ExecuteItems()
        {
            List<InitWrapperItemBase> newInits = new List<InitWrapperItemBase>();
            for (int i = 0; i < _toInitialize.Count; i++)
            {
                bool toInit = true;
                for(int i2 = 0; i2 < _toInitialize[i].runtimeDependences.Count; i2++)
                {
                    if (_toInitialize[i].runtimeDependences[i2].IsEnabled && !_initializedItems.Contains(_toInitialize[i].runtimeDependences[i2]))
                    {
                        toInit = false;
                        break;
                    }
                }
                if (toInit)
                {
                    newInits.Add(_toInitialize[i]);
                }
                else
                {
                    _toInitialize[i].initState = InitWrapperItemBase.InitState.WAITING_DEPENDENCE;
                }
            }
            count += newInits.Count;
            for (int i = 0; i < newInits.Count; i ++)
            {
                InitItem(newInits[i]);
            }
        }

        int count = 0;

        protected void InitItem(InitWrapperItemBase item)
        {
        
            _toInitialize.Remove(item);
            _initializingItems.Add(item);
            item.initState = InitWrapperItemBase.InitState.INITIALIZING;
            item.OnFinish += HandleItemFinish;
            item.Init();
        }
        protected void HandleItemFinish(InitWrapperItemBase item)
        {
            item.OnFinish -= HandleItemFinish;
            _initializingItems.Remove(item);
            _initializedItems.Add(item);
            item.initState = InitWrapperItemBase.InitState.INITIALIZED;
            count--;
            if (count == 0)
            {
                if (Finished)
                {
                    FinishInit();
                }
                else
                {
                    ExecuteItems();
                }
            }
        }
        protected bool Finished
        {
            get
            {
                return _initializingItems.Count == 0 && _toInitialize.Count == 0 && _initializedItems.Count == initializeCount;
            }
        }
        public void FinishInit()
        {
            _endEvt.Execute();
        }
    }
}