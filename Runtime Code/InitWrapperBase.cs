﻿using UnityEngine;

namespace com.FDT.InitializationHandler
{
    public abstract class InitWrapperBase : MonoBehaviour, IInitWrapper
    {

        // Use this for initialization
        public virtual void Init()
        {
            Debug.Log(gameObject.name + " init");
        }

        // Update is called once per frame
        public virtual void FinishInit()
        {

        }
    }
}