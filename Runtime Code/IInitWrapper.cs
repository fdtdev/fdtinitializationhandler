﻿namespace com.FDT.InitializationHandler
{
    public interface IInitWrapper:IInitializable
    {
        void FinishInit();
    }
}