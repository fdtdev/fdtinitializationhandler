﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.InitializationHandler
{
    [DisallowMultipleComponent]
    public abstract class InitWrapperItemBase : MonoBehaviour, IInitWrapper
    {
        public enum InitState
        {
            NOT_INITIALIZED, WAITING_DEPENDENCE, INITIALIZING, INITIALIZED
        }
        public enum AvailableState
        {
            NOT_SET, AVAILABLE, NOT_AVAILABLE
        }
        [SerializeField] protected List<InitWrapperItemBase> _dependences = new List<InitWrapperItemBase>();
        public List<InitWrapperItemBase> dependences { get { return _dependences; } }
        
        public List<InitWrapperItemBase> runtimeDependences = new List<InitWrapperItemBase>();
        
        public System.Action<InitWrapperItemBase> OnFinish;

        public InitState initState = InitState.NOT_INITIALIZED;
        public AvailableState availableState = AvailableState.NOT_SET;

        public void ComputeRuntimeDependences()
        {
            runtimeDependences.Clear();
            
            for (int i = 0; i < _dependences.Count; i++)
            {
                
                DoComputeRuntimeDependences(_dependences[i]);
            }
        }

        private void DoComputeRuntimeDependences(InitWrapperItemBase dependence)
        {
            runtimeDependences.Add(dependence);
            //bool changed = false;
            for (int i = 0; i < dependence._dependences.Count; i++)
            {
                if (!runtimeDependences.Contains(dependence._dependences[i]))
                {
                    //changed = true;
                    DoComputeRuntimeDependences(dependence._dependences[i]);
                }
            }
        }

        public abstract void Init();

        public bool IsEnabled
        {
            get { return enabled && gameObject.activeInHierarchy; }
        }
        private void OnEnable()
        {
            
        }

        public virtual void FinishInit()
        {
            if (OnFinish != null)
                OnFinish(this);
        }       
    }
}