﻿namespace com.FDT.InitializationHandler
{
    public interface IInitializable
    {
        void Init();
    }
}