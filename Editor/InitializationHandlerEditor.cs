﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using com.FDT.InitializationHandler;
using com.FDT.Common.RotorzReorderableList;

namespace com.FDT.InitializationHandler.Editor
{
    [CustomEditor(typeof(InitializationHandler))]
    public class InitializationHandlerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "_initializationItems", "m_Script");
            ReorderableListGUI.Title("Items");
            ReorderableListGUI.ListField(serializedObject.FindProperty("_initializationItems"));
            ReorderableListGUI.Title("On Finish Initialization Event");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_endEvt"), true);
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            serializedObject.ApplyModifiedProperties();
        }
    }   
}