﻿using UnityEngine;
using UnityEditor;

namespace com.FDT.InitializationHandler.Editor
{
    [CustomPropertyDrawer(typeof(InitWrapperItemBase))]
    public class InitWrapperItemBaseDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            bool small = false;
            float lblw = 120;
            if (position.width - lblw < 300)
            {
                small = true;
                lblw = 50;
            }
            EditorGUI.BeginProperty(position, label, property);
            
            Rect r1 = new Rect(position.x, position.y, position.width - lblw, position.height);
            Rect r2 = new Rect(position.x + position.width - lblw, position.y, lblw, position.height);
            
            var obj = property.objectReferenceValue as InitWrapperItemBase;
            bool active = false;
            
            if (property.objectReferenceValue != null)
            {
                active = obj.IsEnabled;
                
                SerializedObject o = new SerializedObject(property.objectReferenceValue);
                SerializedProperty s = o.FindProperty("initState");
                SerializedProperty r = o.FindProperty("availableState");
                string lbl = string.Empty;
                Color oldC = GUI.backgroundColor;
                Color curC = GUI.backgroundColor;
                /*
                 public enum InitState
            {
                NOT_INITIALIZED = 0, WAITING_DEPENDENCE = 1, INITIALIZING = 2, INITIALIZED = 3
            }
                 */
                switch ((InitWrapperItemBase.InitState)s.intValue)
                {
                    case InitWrapperItemBase.InitState.NOT_INITIALIZED:
                        lbl = small ? "-" : "OFF";
                        curC = Color.red;
                        break;
                    case InitWrapperItemBase.InitState.INITIALIZING:
                        lbl = small ? "%" : "INITIALIZING";
                        curC = Color.yellow;
                        break;
                    case InitWrapperItemBase.InitState.WAITING_DEPENDENCE:
                        lbl = small ? "..." : "WAITING";
                        curC = Color.grey;
                        break;
                    case InitWrapperItemBase.InitState.INITIALIZED:
                        lbl = small ? "done" : "INITIALIZED";
                        if ((InitWrapperItemBase.AvailableState)r.intValue == InitWrapperItemBase.AvailableState.AVAILABLE)
                            curC = Color.green;
                        else
                            curC = Color.red;
                        break;
                }
                GUI.backgroundColor = curC;
                EditorGUI.LabelField(r2, lbl, EditorStyles.miniButton);
                GUI.backgroundColor = oldC;
                EditorGUI.EndDisabledGroup();
            }
            
            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = active?GUI.backgroundColor:Color.grey;
            EditorGUI.PropertyField(r1, property, new GUIContent(string.Empty, "ScriptableObject file for the Plugin Wrapper"));
            GUI.backgroundColor = oldColor;
            
            EditorGUI.EndProperty();
        }
    }
}